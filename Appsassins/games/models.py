from django.db import models
from django.contrib.auth.models import User
from accounts.models import AssassinsUser
# Create your models here.

class Game(models.Model):
	name = models.CharField(max_length=30)
	active = models.BooleanField(default=False)
	admin = models.ForeignKey(AssassinsUser)

	def __str__(self):
		return self.name

class Player(models.Model):
	codename = models.CharField(max_length=30)
	active = models.BooleanField(default=False)
	target = models.ForeignKey('self', default=1)
	assassins_user = models.ForeignKey(AssassinsUser)
	game = models.ForeignKey(Game)

	def __str__(self):
		return self.codename

class Kill(models.Model):
	pending = models.BooleanField(default=False)
	timestamp = models.FloatField()
	kill_when = models.TextField()
	kill_where = models.TextField()
	kill_story = models.TextField()
	killer = models.ForeignKey(Player, related_name="killer")
	victim = models.ForeignKey(Player, related_name="victim")
	game = models.ForeignKey(Game)

