import datetime
import time

from django.shortcuts import render
from django import http

from accounts.models import AssassinsUser
from games.models import Game, Player
from django.utils import dateparse
from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse
from random import shuffle
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

# import dateutil.parser
def home(request, game_id):
    game = Game.objects.get(id=game_id)
    user = AssassinsUser.objects.get(id=request.user.id)
    player = Player.objects.filter(game=game).filter(assassins_user=user)
    if not player:
    	ingame = False
    else:
    	ingame = True
    return render(request, 'games/index.html', {
        'game' : game,
        'ingame' : ingame
      })

# given: kill_where, kill_when, kill_story
# infer: timestamp, target, game, killer
@login_required
def submit_kill(request):
    # first, create a new kill
    kill = Kill(timestamp=time.time(), kill_where=request.POST['kill_where'])
    kill.kill_when = request.POST['kill_when']
    kill.kill_story = request.POST['kill_story']
    game_id = request.POST['game_id']
    kill.game = Game.objects.get(pk=game_id)
    kill.killer = Player.get(assassins_user=request.user.assassinsuser, game=kill.game)
    kill.victim = kill.killer.target

    kill.killer.target = kill.victim.target

    kill.killer.save();
    kill.save();

    return render(request, 'games/index.html', {
    })

def newsfeed(request):
   return render(request, 'games/newsfeed.html', {
        'game_name': "YOLOSWAG",
      })

# create a new player for a game
@login_required
def create_player(request):
  if request.method == 'POST':
        codename = request.POST['codename']
        game_id = request.POST['game_id']
        game = Game.objects.get(id=game_id)
        assassins_user = AssassinsUser.objects.get(id=request.user.id)
        player = Player(codename=codename, assassins_user=assassins_user, game=game)
        player.save()
        return render(request, 'games/index.html', {
    	})
  else:
        resp = HttpResponse()
        resp.status_code = 404
        return resp

def assign_targets(request):
  if request.method == 'POST':
        gamename = request.POST['game_name']
        game = Game.objects.get(name=gamename)
        players = Player.objects.filter(game=game)
        player_ids = []
        for player in players:
        	player_ids.append(player.id)
        return HttpResponse()
  else:
        resp = HttpResponse()
        resp.status_code = 404
        return resp
