from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'accounts.views.home'),
    url(r'^accounts/login/$', 'accounts.views.login_view'),
    url(r'^accounts/logout/$', 'accounts.views.logout_view'),
    url(r'^accounts/create_user$', 'accounts.views.create_user'),
    url(r'^accounts/handle_login$', 'accounts.views.handle_login'),
    url(r'^newsfeed/$', 'games.views.newsfeed'),
    url(r'^create_game/$', 'accounts.views.create_game'),
    url(r'^games/(?P<game_id>[0-9]+)/$', 'games.views.home'),
    url(r'^submit_kill/$', 'games.views.submit_kill'),
    url(r'^create_player/$', 'games.views.create_player'),
)
