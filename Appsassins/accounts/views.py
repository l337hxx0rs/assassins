from django import http
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from accounts.models import AssassinsUser
from games.models import Game

# page showing login / create account information
def login_view(request):
  return render(request, 'accounts/login.html')

# view handling incoming login requests
def handle_login(request):
  username = request.POST['email']
  password = request.POST['password']
  user = authenticate(username=username, password=password)
  if user is not None:
    if user.is_active:
      login(request, user)
      # Redirect to a success page.
      return redirect('accounts.views.home')
    else:
      # Return a 'disabled account' error message
      return redirect('accounts.views.login_view')
  else:
    # Return an 'invalid login' error message.
    return redirect('accounts.views.login_view')


# logout account
def logout_view(request):
  logout(request)
  return redirect('accounts.views.login_view')

def create_user(request):
    email = request.POST.get("email")
    password = request.POST.get("password");
    firstname = request.POST.get("firstname");
    lastname = request.POST.get("lastname");

    # check if email exists in our database
    if User.objects.filter(email=email).exists():
        return redirect('accounts.views.login_view')

    # create user
    user = User.objects.create_user(email, email, password, first_name=firstname, last_name=lastname)
    user.save();

    auser = AssassinsUser(user=user, id=user.id)
    auser.save()

    user = authenticate(username=email, password=password)
    if user is not None:
      if user.is_active:
        login(request, user)
        # Redirect to a success page.
        return redirect('accounts.views.home')
      else:
        # Return a 'disabled account' error message
        return redirect('accounts.views.login_view')
    else:
      # Return an 'invalid login' error message.
      return redirect('accounts.views.login_view')

@login_required
def home(request):
    # get account data here ...
    admin = AssassinsUser.objects.get(id=request.user.id)
    admin_games = Game.objects.filter(admin=admin)
    return render(request, 'accounts/index.html', {'admin_games':admin_games})

# start a new game button
@login_required
def create_game(request):
    if request.method == 'POST':
        name = request.POST['game_name']
        admin = AssassinsUser.objects.get(id=request.user.id)
        game = Game(name=name, admin=admin)
        game.save()
        return redirect('games.views.home', game_id=game.id)
    else:
        resp = HttpResponse()
        resp.status_code = 404
        return resp
