from django.db import models
from django.contrib.auth.models import User
#from games.models import Player

# Create your models here.
class AssassinsUser(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)
    #players = models.ForeignKey(Player)
    #admins = models.OneToManyField(Admin)
    #invites = models.OneToManyField(Game)

    def __unicode__(self):
        return self.user.username
